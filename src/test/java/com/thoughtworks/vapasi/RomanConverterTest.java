package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RomanConverterTest {

    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @Test
    void shouldConvertI() {
    }

    @Test
    void shouldConvertII() {
    }

    @Test
    void shouldConvertIII() {
    }

    @Test
    void shouldConvertIV() {
    }

    @Test
    void shouldConvertV() {
    }

    @Test
    void shouldConvertVI() {
    }

    @Test
    void shouldConvertVII() {
    }

    @Test
    void shouldConvertIX() {
    }

    @Test
    void shouldConvertX() {
    }

    @Test
    void shouldConvertXXXVI() {
    }

    @Test
    void shouldConvertMMXII() {
    }

    @Test
    void shouldConvertMCMXCVI() {
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
    }
}